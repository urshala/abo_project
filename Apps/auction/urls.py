from django.conf.urls import url


from . import views

app_name='auction'

urlpatterns = (
	url(r'^$', views.auction_list, name='index_view'),
	url(r'^auctions/$', views.auction_list, name='auction_list'),
	url(r'^auctions/detail/(?P<pk>\d+)/$', views.auction_detail, name='auction_detail'),
	url(r'^auctions/detail/(?P<pk>\d+)/edit/$', views.edit_auction, name='edit_auction'),
	url(r'^auctions/detail/(?P<pk>\d+)/delete/$', views.delete_auction, name='delete_auction'),
	url(r'^auctions/detail/(?P<pk>\d+)/bid/$',views.bid_on_auction, name='bid_on_auction'),
	url(r'^auctions/detail/(?P<pk>\d+)/bid/list/$', views.get_list_of_bids),
	url(r'^auctions/(?P<user>\w+)/auctions-list/$',views.auction_list_by_user, name='profile_auction_list'),
	url(r'^auctions/search/$', views.search_view, name='search_auction'),
	url(r'^auctions/create/$', views.create_auction, name='create_auction'),
	url(r'^auctions/create/confirm/$', views.confirm_auction, name='confirm_auction'),

	



	)