# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-01 14:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='auction',
            old_name='minimum_price',
            new_name='start_price',
        ),
        migrations.RemoveField(
            model_name='auction',
            name='price_now',
        ),
    ]
