from __future__ import unicode_literals
from django.dispatch import receiver
from django.db.models.signals import post_save

from datetime import datetime, timedelta
from django.db import models
from django.contrib.auth.models import User
from django.core.mail import send_mail

# Create your models here.
AUCTION_STATUS = (
	('Act', 'Active'),
	('Ban', 'Banned'),
	('Due', 'Due'),
	('Fin', 'Adjundicated')
	)



def get_deadline():
	return datetime.today() + timedelta(hours=24) 

class Auction(models.Model):
	'''Model for auction product/item'''
	seller = models.ForeignKey(User)
	title = models.CharField(max_length=120)
	description = models.TextField()
	start_price = models.PositiveIntegerField()
	created_at = models.DateTimeField(auto_now_add= True)
	deadline = models.DateTimeField(default= get_deadline)
	status = models.CharField(max_length=3, choices=AUCTION_STATUS, default='Act')

	def save(self, *args, **kwargs):
		if not self.start_price:
			self.start_price = self.minimum_price
		super(Auction, self).save(*args, **kwargs)

	def __str__(self):
		return self.title


@receiver(post_save, sender=Auction)
def send_confirmation_email(sender, *args, **kwargs):
	'''sends an email to the user notifying that the auction has been created'''
	if kwargs.get('created', False):
		# send_mail('Subject',
		# 	'Message Body',
		# 	'pythonkhata@gmail.com',
		# 	[kwargs['instance'].seller.email],
		# 	fail_silently = False

		# 	)
		print 'email sent '



class Bid(models.Model):
	bidder = models.ForeignKey(User, on_delete=models.CASCADE)
	product = models.ForeignKey(Auction, on_delete=models.CASCADE, related_name='product')
	#amount_before_bid = models.PositiveIntegerField()
	increase_by = models.PositiveIntegerField()
	amount_after_bid = models.PositiveIntegerField()
	bid_time = models.DateTimeField(auto_now=True)

	

	def __str__(self):
		return self.bidder.username

		




	
