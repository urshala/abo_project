from django import forms
from .models import Auction, Bid


class AuctionForm(forms.ModelForm):
	class Meta:
		model = Auction
		seller = Auction.seller
		deadline = forms.DateTimeField()
		fields = ('title', 'description', 'start_price','deadline')
		widgets = {
			'title': forms.TextInput(attrs={'class':'form-control'}),
			'description' : forms.Textarea(attrs= {'class' : 'form-control'}),
			'start_price' : forms.NumberInput(attrs= {'class' : 'form-control'}),
			'deadline' : forms.DateTimeInput(attrs= {'class' : 'form-control'})
		}
		

class BidForm(forms.ModelForm):
	class Meta:
		model = Bid
		fields = ('increase_by',)