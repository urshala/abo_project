from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.urls import reverse
from .forms import AuctionForm, BidForm

from django.contrib.auth.decorators import login_required


from .models import Auction, Bid

# Create your views here.


def index_view(request):
	return render(request, 'auction/index.html')



def auction_list(request):
	auctions = Auction.objects.all()
	return render(request, 'auction/auction_list.html', {'auction_list':auctions})


def auction_list_by_user(request, user):
	'''Returns all the lists of auctions by filtered user'''
	auctions = get_list_or_404(Auction, seller__username=user)
	return render(request, 'auction/auction_list_user.html', {'auctions':auctions})



def auction_detail(request, pk):
	'''Gets the detail of auction instance where the primary key matches with the passed pk'''
	auction = get_object_or_404(Auction, pk=pk)
	latest_high_bid_amount = get_current_bid_amount(request,pk)

	return render(request, 'auction/auction_detail.html', {'auction':auction, 'current_price':latest_high_bid_amount })



def create_auction(request):
	'''Display the form to create an auction instance and send the form to confirm_auction()
		where it asks for confirmation to whether save it or not'''
	errors = ''
	if request.method == 'GET':
		form = AuctionForm()
	elif request.method == 'POST':
		form = AuctionForm(request.POST)
		if form.is_valid():
			return render(request, 'auction/confirmation_form.html', {'form':form})
		else:
			errors = form.errors
	return render(request, 'auction/create_auction.html', {'form':form, 'errors':errors})


def delete_auction(request,pk):
	'''Deletes the selected auction instnce'''
	try:
		auction = Auction.objects.get(pk=pk)
	except Auction.DoesNotExist:
		print 'The item you asked for doesnt exist'
	auction.delete()
	messages.add_message(request, messages.SUCCESS, 'Successfully deleted auction item')
	return HttpResponseRedirect(reverse('auction:auction_list'))


def confirm_auction(request):
	'''Get the passed post data from create_auction() and re-desplay the form with filled data
		and save it if confirm_submit is in POST data which is passed thru form'''
	if request.method == 'POST' and 'confirm_submit' in request.POST:
		form = AuctionForm(request.POST)
		if form.is_valid():
			unconfirmed_form = form.save(commit=False)
			unconfirmed_form.seller = request.user
			unconfirmed_form.save()
			messages.add_message(request, messages.SUCCESS, 'Successfully created new auction')
			return HttpResponseRedirect(reverse('auction:auction_list'))
		else:
			print form.errors
	return auction_list(request)


def edit_auction(request,pk):
	'''Edit the auction instance that a user belongs to'''
	auction = Auction.objects.get(pk=pk, seller=request.user)
	submitted_auction_form = AuctionForm(instance=auction)
	if request.method == 'POST':
		submitted_auction_form = AuctionForm(request.POST, instance=auction)
		if submitted_auction_form.is_valid():
			submitted_auction_form.save()
			messages.add_message(request, messages.SUCCESS, 'Your auction item is updated')
			return HttpResponseRedirect(reverse('auction:auction_list'))
		else:
			errors = submitted_auction_form.errors
	return render(request, 'auction/edit_auction.html', {'form':submitted_auction_form}) 


def get_current_bid_amount(request,pk):
	'''Returns the current highest bid amount related to auction instance
		Using backward relation lookup; that related_name = product. First see if bid
		instance exists. If yes, get the latest bid instance related to this auction 
		instance else get the initial amount'''
	auction = get_object_or_404(Auction, pk=pk)
	if auction.product.exists():
		latest_high_bid_amount = auction.product.latest('pk').amount_after_bid
	else:
		latest_high_bid_amount = auction.start_price
	return latest_high_bid_amount




def bid_on_auction(request,pk):
	'''Get a POST data of bid amount (increased_by) and increase the value by adding that to 
		latest highest bid amount and thus save the model instance'''
	product = Auction.objects.get(pk=pk)
	latest_high_bid_amount = get_current_bid_amount(request,pk)
	if request.method == 'GET':
		form = BidForm()
	if request.user == product.seller:
		return render(request, 'auction/404.html', {'message': 'You cannot bid on your own auction'})

	if request.method == 'POST':
		form = BidForm(request.POST)
		if form.is_valid():
			unconfirmed_form = form.save(commit=False)
			unconfirmed_form.product = product
			unconfirmed_form.bidder = request.user
			increase_by = request.POST['increase_by']
			unconfirmed_form.increase_by = increase_by
			unconfirmed_form.amount_after_bid = int(increase_by) + latest_high_bid_amount
			unconfirmed_form.save()
			messages.add_message(request,messages.SUCCESS, 'You have successfully bidded on %s' %product.title)
			return HttpResponseRedirect(reverse('auction:auction_list'))
		else:
			errors = form.errors
	return render(request, 'auction/bid_form.html', {'form':form, 'product':product, 'current_amount':latest_high_bid_amount})


def get_list_of_bids(request,pk):
	bids = Bid.objects.filter(bidder=request.user, product=pk)
	for b in bids:
		print b.amount_after_bid
	return HttpResponse('H')





def search_view(request):
	'''Gets the seearch words and display the matching results'''
	#I should try it via ajax
	searchwords = request.GET['srch-term']
	#print 'The input words were' ,searchwords
	search_results = Auction.objects.filter(title__icontains=searchwords)
	
	#return HttpResponse(search_results)
	messages.add_message(request, messages.SUCCESS, 'Your search for %s yielded following results' %searchwords)
	return render(request,'auction/search_results.html', {'results':search_results})
