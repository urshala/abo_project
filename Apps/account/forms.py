from django import forms
from django.contrib.auth.models import User


class UserRegistrationForm(forms.ModelForm):
	#password = forms.CharField(max_length=25, label="Password", widget=forms.PasswordInput)
	class Meta:
		model = User
		fields = ('username', 'password', 'email')
		help_texts = {
			'username' : None,
		}
		widgets = {
		'username' : forms.TextInput(attrs={'class' : 'form-control'}),
		'password' : forms.PasswordInput(attrs={'class' : 'form-control'}),
		'email' : forms.EmailInput(attrs={'class' : 'form-control'})
		}

