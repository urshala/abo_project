from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
from django.contrib.auth.models import User

from .forms import (
	UserRegistrationForm,
	)

def user_registration(request):
	if request.method == 'GET':
		form = UserRegistrationForm()
	if request.method == 'POST':
		form = UserRegistrationForm(request.POST)
		if form.is_valid():
			user = form.save()
			user.set_password(user.password)
			user.save()
			return HttpResponse('User created')
	return render(request, 'account/user_registration.html', {'form':form})


def user_login(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username, password=password)
		if user is not None:
			login(request,user)
			return HttpResponseRedirect(reverse('auction:index_view'))
		return HttpResponse('Your login credentials are not correct')
	return render(request,'account/user_login.html')


@login_required
def user_logout(request):
	logout(request)
	return HttpResponseRedirect(reverse('auction:index_view'))


@login_required
def get_user_profile(request):
	"""Show the html page with links to their profile"""
	return render(request, 'account/user_profile.html')



@login_required
def user_profile_change(request):
	user = User.objects.get(username=request.user)
	if request.method=='POST':
		form = UserRegistrationForm(request.POST, instance=user)
		if form.is_valid():
			user = form.save()
			user.set_password(user.password)
			user.save()
			return HttpResponse('User Modified')
	else:
		form = UserRegistrationForm(request.POST or None, instance=user)

	return render(request, 'account/user_registration.html', {'form':form, 'edit_form':True})
