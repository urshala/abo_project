from django.conf.urls import url


from .views import (
	user_registration,
	user_login,
	user_profile_change,
	user_logout,
	get_user_profile


	)

app_name = 'account'
urlpatterns = [

	url(r'^register/$', user_registration, name='user_register'),
	url(r'^login/$', user_login, name='user_login'),
	url(r'^profile/user/$', get_user_profile, name='user_profile'),
	url(r'^edit/$', user_profile_change, name='edit_profile'),
	url(r'^logout/$', user_logout, name='user_logout'),
]