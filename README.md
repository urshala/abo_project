# README #
This is a simple auction based website that lets user upload the contents and user can bid on the items uploaded by other users. 

There is time limit for how long the auction will be valid, once the auction are invalid or expire, they automatically get deleted from the system. 

The user and owner of auction get email notification once there is activity on the auction relating to them. 

See the requirements.txt file for packages required.